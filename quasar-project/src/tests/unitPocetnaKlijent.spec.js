import { mount } from '@vue/test-utils'
import PocetnaKlijent from 'src/pages/PocetnaKlijent.vue'

describe('PocetnaKlijent', () => {
  it('rederira komponentu', () => {
    const wrapper = mount(PocetnaKlijent)

    // Assert the presence of specific elements in the template
    expect(wrapper.find('.my-card')).toBeTruthy();
    expect(wrapper.find('.q-carousel')).toBeTruthy();
    expect(wrapper.find('.text-h6')).toBeTruthy();
    expect(wrapper.find('.text-subtitle2')).toBeTruthy();
    expect(wrapper.find('.text-h5')).toBeTruthy();
    expect(wrapper.find('q-select')).toBeTruthy();
    expect(wrapper.find('.q-btn[label="Spremi"]')).toBeTruthy();
    expect(wrapper.find('.q-timeline')).toBeTruthy();
  });

  it('azurira select opciju kada je trener odabran', async () => {
    const wrapper = mount(PocetnaKlijent)

    // Simuliraj opciju odabira trenera
    await wrapper.find('q-select').vm.$emit('change', 'trainer-id-1');

    expect(wrapper.vm.selectedOption).toBe('trainer-id-1');
  });
  //otvaranje stranice
  it('otvara fitka stranicu kad se klikne na nju', () => {
    const windowOpenMock = jest.fn();
    window.open = windowOpenMock;

    const wrapper = mount(PocetnaKlijent)
    wrapper.find('.q-btn[label="Otvori"]').trigger('click');

    expect(windowOpenMock).toHaveBeenCalledWith('https://fitka.hr/', '_blank');
  });

  //otvaranje stranice
  it('Otvara Budi fit stranicu kad se klikne na nju', () => {
    const windowOpenMock = jest.fn();
    window.open = windowOpenMock;

    const wrapper = mount(PocetnaKlijent)
    wrapper.find('.q-btn[label="Otvori"]').trigger('click');

    expect(windowOpenMock).toHaveBeenCalledWith('https://budi-fit.hr/fitness-blog/', '_blank');
  });
  //testiranje refresha
  it('refresha stranicu kad je klinut gumb za refresh', () => {
    const locationReloadMock = jest.fn();
    global.location.reload = locationReloadMock;

    const wrapper = mount(PocetnaKlijent);
    wrapper.find('.q-btn[label="Osvježi"]').trigger('click');

    expect(locationReloadMock).toHaveBeenCalled();
  });
  //ažuriranje selecta trenera kada je nvi trener odabran
  it('azurira vrijednost q-select komponente kada je odabrana nova opcija', async () => {
    const wrapper = mount(PocetnaKlijent);
  
    // Simuliraj promjenu vrijednosti u q-select komponenti
    await wrapper.find('q-select').setValue('nova-vrijednost');
  
    // Očekuj da je vrijednost q-select komponente je ažurirana
    expect(wrapper.vm.selectedValue).toBe('nova-vrijednost');

  });
  
});