import { mount } from '@vue/test-utils';
import TrenerPodacioKlijentu from 'src/pages/TrenerPodacioKlijentu.vue';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import { getDoc } from 'firebase/firestore';

jest.mock('firebase/auth', () => ({
  getAuth: jest.fn(),
  onAuthStateChanged: jest.fn(),
}));

jest.mock('firebase/firestore', () => ({
  getDoc: jest.fn(),
}));

describe('TrenerPodacioKlijentu', () => {
  let wrapper;

  beforeEach(() => {
    getAuth.mockReturnValue({
      currentUser: {
        uid: 'test-trainer-id',
      },
    });

    onAuthStateChanged.mockImplementation((auth, callback) => {
      callback({
        uid: 'test-trainer-id',
      });
    });

    getDoc.mockReturnValue({
      exists: true,
      data: () => ({
        ime_korisnika: 'John',
        prezime_korisnika: 'Doe',
        kontakt_korisnika: '123456789',
        email: 'john.doe@example.com',
        shape: 'male',
        tezina_korisnika: '80',
        visina_korisnika: '180',
        datumrod_korisnika: '1990-01-01',
      }),
    });

    wrapper = mount(TrenerPodacioKlijentu, {
      mocks: {
        $route: {
          params: {
            id: 'test-client-id',
          },
        },
      },
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('dohvaća klijentske podatke', async () => {
    expect(getAuth).toHaveBeenCalled();
    expect(onAuthStateChanged).toHaveBeenCalled();
    expect(getDoc).toHaveBeenCalledWith(expect.any(Object), 'Treneri/test-trainer-id/Clients/test-client-id');
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.clientData).toEqual({
      ime_korisnika: 'John',
      prezime_korisnika: 'Doe',
      kontakt_korisnika: '123456789',
      email: 'john.doe@example.com',
      shape: 'male',
      tezina_korisnika: '80',
      visina_korisnika: '180',
      datumrod_korisnika: '1990-01-01',
    });
  });

  it('ispravno prikazuje osobne podatke klijenta', () => {
    const imeInput = wrapper.find('input[data-testid="ime-korisnika"]');
    const prezimeInput = wrapper.find('input[data-testid="prezime-korisnika"]');
    const kontaktInput = wrapper.find('input[data-testid="kontakt-korisnika"]');
    const emailInput = wrapper.find('input[data-testid="email"]');
    const shapeInput = wrapper.find('input[data-testid="shape"]');
    const tezinaInput = wrapper.find('input[data-testid="tezina-korisnika"]');
    const visinaInput = wrapper.find('input[data-testid="visina-korisnika"]');
    const datumrodInput = wrapper.find('input[data-testid="datumrod-korisnika"]');
  
    expect(imeInput.element.value).toBe('John');
    expect(prezimeInput.element.value).toBe('Doe');
    expect(kontaktInput.element.value).toBe('123456789');
    expect(emailInput.element.value).toBe('john.doe@example.com');
    expect(shapeInput.element.value).toBe('male');
    expect(tezinaInput.element.value).toBe('80');
    expect(visinaInput.element.value).toBe('180');
    expect(datumrodInput.element.value).toBe('1990-01-01');
  });
  

  it('validira polja', async () => {
    const saveButton = wrapper.find('button[data-testid="save-button"]');
    expect(saveButton.attributes('disabled')).toBeTruthy();

    await wrapper.setData({
      clientData: {
        ime_korisnika: 'John',
        prezime_korisnika: 'Doe',
        kontakt_korisnika: '123456789',
        email: 'invalid-email',
        shape: 'male',
        tezina_korisnika: '80',
        visina_korisnika: '180',
        datumrod_korisnika: '1990-01-01',
      },
    });

    expect(saveButton.attributes('disabled')).toBeTruthy();

    await wrapper.setData({
      clientData: {
        ime_korisnika: 'John',
        prezime_korisnika: 'Doe',
        kontakt_korisnika: '123456789',
        email: 'john.doe@example.com',
        shape: 'male',
        tezina_korisnika: '80',
        visina_korisnika: '180',
        datumrod_korisnika: '1990-01-01',
      },
    });

    expect(saveButton.attributes('disabled')).toBeFalsy();
  });
});
