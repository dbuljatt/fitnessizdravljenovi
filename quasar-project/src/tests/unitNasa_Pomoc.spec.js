import { shallowMount } from '@vue/test-utils';
import Nasa_Pomoc from 'src/pages/Nasa_Pomoc.vue';
import { expect } from 'chai';

describe('Nasa_Pomoc', () => {
  it('provjerava ispravno ponašanje metode beforeMount()', () => {
    const localStorageMock = {
      getItem: jest.fn(),
      setItem: jest.fn(),
      removeItem: jest.fn(),
    };
    global.localStorage = localStorageMock;
    global.location = { reload: jest.fn() };

    // Montiranje komponente
    const wrapper = shallowMount(Nasa_Pomoc);

    // Provjera pozivanja metoda localStorage-a
    expect(localStorage.getItem).to.have.been.calledWith('isReloaded');

    // Provjera pozivanja metoda location.reload()
    expect(location.reload).to.have.been.called;

    // Provjera pozivanja metoda localStorage-a
    expect(localStorage.setItem).to.have.been.calledWith('isReloaded', 'true');

    // Provjera uklanjanja stavke iz localStorage-a
    expect(localStorage.removeItem).to.have.been.calledWith('isReloaded');
  });
});
