import { mount } from '@vue/test-utils';
import MainLayout from 'src/layouts/MainLayout.vue';

describe('MainLayout', () => {
  test('navigira se prema ruti koja je kliknuta', async () => {
    const mockRouter = {
      push: jest.fn()
    };
    const wrapper = mount(MainLayout, {
      global: {
        mocks: {
          $router: mockRouter
        }
      }
    });
    const helpIcon = wrapper.find('.q-icon[name="help"]');

    // Klikni na ikonu pomoći
    await helpIcon.trigger('click');

    // Provjeri da li je pozvana odgovarajuća ruta
    expect(mockRouter.push).toHaveBeenCalledWith('/trener/Nasa_Pomoc');
  });

  test('navigira se prema ruti kada je ikona kliknuta', async () => {
    const mockRouter = {
      push: jest.fn()
    };
    const wrapper = mount(MainLayout, {
      global: {
        mocks: {
          $router: mockRouter
        }
      }
    });
    const calculatorIcon = wrapper.find('.q-icon[src*="calculator.png"]');

    // Klikni na ikonu kalkulatora
    await calculatorIcon.trigger('click');

    // Provjeri da li je pozvana odgovarajuća ruta
    expect(mockRouter.push).toHaveBeenCalledWith('/trener/NasKalkulator');
  });

  test('navigira se prema ruti koja je kliknuta', async () => {
    const mockRouter = {
      push: jest.fn()
    };
    const mockSignOut = jest.fn();
    const wrapper = mount(MainLayout, {
      global: {
        mocks: {
          $router: mockRouter,
          $signOut: mockSignOut
        }
      }
    });
    const signOutIcon = wrapper.find('.q-icon[src*="sign-out.png"]');

    // Klikni na ikonu odjave
    await signOutIcon.trigger('click');

    // Provjeri da li je pozvana odgovarajuća ruta
    expect(mockSignOut).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith('/login');
  });

  test('prikazuje avatara bairanog prema spolu', async () => {
    const mockUser = {
      uid: 'user-123',
      email: 'test@example.com',
      gender: 'Muško'
    };
    const wrapper = mount(MainLayout, {
      global: {
        mocks: {
          $user: mockUser
        }
      }
    });

    // Provjeri da li se prikazuje avatar za muško
    const maleAvatar = wrapper.find('.q-avatar[src="https://cdn.quasar.dev/img/boy-avatar.png"]');
    expect(maleAvatar.exists()).toBe(true);

    // Promijeni spol na žensko
    mockUser.gender = 'Žensko';

    // Pričekaj zahtjev za ažuriranjem avatara
    await wrapper.vm.$nextTick();

    // Provjeri da li se prikazuje avatar za žensko
    const femaleAvatar = wrapper.find('.q-avatar[src="https://img.freepik.com/premium-vector/face-cute-girl-avatar-young-girl-portrait-vector-flat-illustration_192760-82.jpg?w=2000"]');
    expect(femaleAvatar.exists()).toBe(true);
  });
});