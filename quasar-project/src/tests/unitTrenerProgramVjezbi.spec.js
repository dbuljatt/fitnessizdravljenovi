import { mount } from '@vue/test-utils';
import TrenerProgramVjezbi from 'src/pages/TrenerProgramVjezbi.vue';

describe('TrenerProgramVjezbi', () => {
  it('adds an exercise on the selected day', async () => {
    const wrapper = mount(TrenerProgramVjezbi);
    const addButton = wrapper.find('.q-btn[label="Dodaj vježbu"]');
    addButton.trigger('click');
    await wrapper.vm.$nextTick();
    const exerciseInputs = wrapper.findAll('.exercise-item q-input');
    expect(exerciseInputs.length).toBe(2); // ako ima vjezba bar jedna
  });

  it('oznaci odabrane dane', async () => {
    const wrapper = mount(TrenerProgramVjezbi);
    const dayItem = wrapper.find('.q-expansion-item__header');
    dayItem.trigger('click');
    await wrapper.vm.$nextTick();
    const dayItemClass = dayItem.classes();
    expect(dayItemClass).toContain('q-expansion-item__header--expanded');
  });

  it('sprema vjezbe za taj dan', async () => {
   
    const mockSubmitExercises = jest.fn();
    const mockRouterPush = jest.fn();
    const wrapper = mount(TrenerProgramVjezbi, {
      global: {
        mocks: {
          $router: {
            push: mockRouterPush,
          },
        },
      },
    });
    wrapper.vm.submitExercises = mockSubmitExercises;

    // Simulira unos vjezbi
    const addButton = wrapper.find('.q-btn[label="Dodaj vježbu"]');
    addButton.trigger('click');
    await wrapper.vm.$nextTick();
    const exerciseInputs = wrapper.findAll('.exercise-item q-input');
    exerciseInputs[0].setValue('Exercise 1');
    exerciseInputs[1].setValue('Exercise description 1');
    const saveButton = wrapper.find('.q-btn[label="Spremi"]');
    saveButton.trigger('click');
    await wrapper.vm.$nextTick();

    expect(mockSubmitExercises).toHaveBeenCalled();
    expect(mockRouterPush).not.toHaveBeenCalled(); // ako nije logiran
  });

  it('pokazuje eror ako vjezba nije upisana ipravno', async () => {
    const wrapper = mount(TrenerProgramVjezbi);
    const addButton = wrapper.find('.q-btn[label="Dodaj vježbu"]');
    addButton.trigger('click');
    await wrapper.vm.$nextTick();
    const exerciseInputs = wrapper.findAll('.exercise-item q-input');
    const saveButton = wrapper.find('.q-btn[label="Spremi"]');
    saveButton.trigger('click');
    await wrapper.vm.$nextTick();
    const errorMessage = wrapper.find('.q-field__messages .q-field__message');
    expect(errorMessage.text()).toBe('Naziv vježbe 1 je obvezno polje'); // nije unio naziv vjezbe
  });
  it('Spremi button je omoguen kad je sve popunjeno', async () => {
    const wrapper = mount(TrenerProgramVjezbi);
    const addButton = wrapper.find('.q-btn[label="Dodaj vježbu"]');
    addButton.trigger('click');
    await wrapper.vm.$nextTick();
  
    const exerciseInputs = wrapper.findAll('.exercise-item q-input');
    const exerciseNameInput = exerciseInputs[0];
    const exerciseDescriptionInput = exerciseInputs[1];
    const saveButton = wrapper.find('.q-btn[label="Spremi"]');
  
    expect(saveButton.props().disabled).toBe(true); // Očekuje se da je gumb "Spremi" onemogućen jer oba polja su prazna
  
    exerciseNameInput.setValue('Vježba 1');
    await wrapper.vm.$nextTick();
    expect(saveButton.props().disabled).toBe(true); // Očekuje se da je gumb "Spremi" i dalje onemogućen jer drugo polje je prazno
  
    exerciseDescriptionInput.setValue('Opis vježbe 1');
    await wrapper.vm.$nextTick();
    expect(saveButton.props().disabled).toBe(false); // Očekuje se da je gumb "Spremi" omogućen jer su oba polja popunjena
  
    exerciseNameInput.setValue('');
    await wrapper.vm.$nextTick();
    expect(saveButton.props().disabled).toBe(true); // Očekuje se da je gumb "Spremi" ponovno onemogućen jer prvo polje je prazno
  });
  it('treba omogućiti gumb za spremanje kada sva tekstualna polja imaju vrijednosti', async () => {
    const wrapper = mount(TrenerPlanPrehrane);

    // Provjera početnog stanja - gumb "Spremi" treba biti onemogućen
    const saveButton = wrapper.find('.q-btn[label="Spremi"]');
    expect(saveButton.attributes().disabled).toBe('true');

    // Unos vrijednosti u sva tekstualna polja
    const mealInputs = wrapper.findAll('.meal-item');
    mealInputs.forEach((mealInput) => {
      const nameInput = mealInput.find('q-input[label^="Naziv jela"]');
      const descriptionInput = mealInput.find('q-input[label^="Opis jela"]');
      nameInput.setValue('Naziv jela');
      descriptionInput.setValue('Opis jela');
    });

    // Provjera stanja nakon unosa vrijednosti - gumb "Spremi" treba biti omogućen
    expect(saveButton.attributes().disabled).toBeFalsy();
  });
  it('treba dodati novu stavku obroka kada se klikne gumb "Dodaj jelo".', async () => {
    const wrapper = mount(TrenerPlanPrehrane);

    // Provjera početnog broja stavki jela
    const initialMealItems = wrapper.findAll('.meal-item');
    expect(initialMealItems.length).toBe(0);

    // Klik na gumb "Dodaj jelo"
    const addButton = wrapper.find('.q-btn[label="Dodaj jelo"]');
    await addButton.trigger('click');

    // Provjera broja stavki jela nakon klika na gumb
    const updatedMealItems = wrapper.findAll('.meal-item');
    expect(updatedMealItems.length).toBe(1);
  });
});

