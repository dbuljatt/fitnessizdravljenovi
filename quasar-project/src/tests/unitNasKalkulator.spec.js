import { mount } from '@vue/test-utils';
import NasKalkulator from 'src/pages/NasKalkulator.vue';

describe('NasKalkulator', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(NasKalkulator);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('ispravno izračunava kalorije kada se klikne gumb "Izračunaj kalorije".', async () => {
    // postavljanje vrijednosti
    await wrapper.setData({
      godine: 30,
      tezina: 70,
      visina: 170,
      spol: { label: 'Muškarac', value: 5 },
      aktivnost: { label: 'Srednje aktivan', value: 1.55 },
      cilj: { label: 'Izgubi težinu (0.5 kg/tjedno)', value: -500 },
    });

    // Klik na "Izračunaj kalorije" gumb
    const calculateButton = wrapper.find('button');
    await calculateButton.trigger('click');

    // Expected calorie calculation
    const expectedBMRMale = 88.362 + (13.397 * 70) + (4.799 * 170) - (5.677 * 30);
    const expectedCalories = Math.round(expectedBMRMale * 1.55) - 500;

    // izračunate kalorije ispravno prikazane
    const caloriesText = wrapper.find('.text-h6');
    expect(caloriesText.text()).toContain(expectedCalories);
  });

  it('onemogućuje gumb "Izračunaj kalorije" kada obrazac nije valjan', async () => {
    // Forma nije validna
    const calculateButton = wrapper.find('button');
    expect(calculateButton.attributes('disabled')).toBeTruthy();

    // Dodavanje podataka kako bi forma bila validna
    await wrapper.setData({
      godine: 30,
      tezina: 70,
      visina: 170,
      spol: { label: 'Muškarac', value: 5 },
      aktivnost: { label: 'Srednje aktivan', value: 1.55 },
      cilj: { label: 'Izgubi težinu (0.5 kg/tjedno)', value: -500 },
    });

    // Provjera ako je gumb omogućen
    expect(calculateButton.attributes('disabled')).toBeFalsy();
  });

  it('poništava ulazne vrijednosti nakon izračuna kalorija', async () => {
    // Postavljanje vrijednosti
    await wrapper.setData({
      godine: 30,
      tezina: 70,
      visina: 170,
      spol: { label: 'Muškarac', value: 5 },
      aktivnost: { label: 'Srednje aktivan', value: 1.55 },
      cilj: { label: 'Izgubi težinu (0.5 kg/tjedno)', value: -500 },
    });

    // Klik na "Izračunaj kalorije" gumb
    const calculateButton = wrapper.find('button');
    await calculateButton.trigger('click');

    // Provjera je li sve vraćeno na null
    expect(wrapper.vm.godine).toBeNull();
    expect(wrapper.vm.tezina).toBeNull();
    expect(wrapper.vm.visina).toBeNull();
    expect(wrapper.vm.spol).toBeNull();
    expect(wrapper.vm.aktivnost).toBeNull();
    expect(wrapper.vm.cilj).toBeNull();
    expect(wrapper.vm.kalorije).toBeNull();
  });

  it('onemogućuje gumb "Izračunaj kalorije" kada su sva polja za unos prazna', async () => {
    // Provjera je li gumb onemogućen
    const calculateButton = wrapper.find('button');
    expect(calculateButton.attributes('disabled')).toBeTruthy();

    // Postavljanje vrijednosti samo za jedno polje
    await wrapper.setData({
      godine: 30,
    });

    // Provjera je li gumb i dalje onemogućen
    expect(calculateButton.attributes('disabled')).toBeTruthy();
  });
});
