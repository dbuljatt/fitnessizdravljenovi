import { mount } from '@vue/test-utils';
import KlijentBira from 'src/pages/KlijentBira.vue';

describe('KlijentBira', () => {
  it('prikazuje opcije za program vježbi i plan prehrane', () => {
    const wrapper = mount(KlijentBira);

    // Provjeri je li prikazan header s naslovom "Trening"
    expect(wrapper.find('h2').text()).toBe('Trening');

    // Provjeri je li prikazana opcija za program vježbi
    const programVjezbiLink = wrapper.find('router-link[to="/klijent/KlijentProgramVjezbi"]');
    expect(programVjezbiLink.exists()).toBe(true);
    expect(programVjezbiLink.find('.absolute-bottom').text()).toBe('Program vježbi');
    expect(programVjezbiLink.find('q-card-section').text()).toContain('Ovdje možete vidjeti Vaš prilagođen plan vježbi!');

    // Provjeri je li prikazana opcija za plan prehrane
    const planPrehraneLink = wrapper.find('router-link[to="/klijent/KlijentPlanPrehrane"]');
    expect(planPrehraneLink.exists()).toBe(true);
    expect(planPrehraneLink.find('.absolute-bottom').text()).toBe('Plan prehrane');
    expect(planPrehraneLink.find('q-card-section').text()).toContain('Ovdje možete vidjeti Vaš prilagođeni plan prehrane!');
  });
});