import { shallowMount } from '@vue/test-utils';
import TrenerovPopisKlijenta from 'src/pages/TrenerovPopisKlijenta.vue';

describe('TrenerovPopisKlijenta', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(TrenerovPopisKlijenta);
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('treba inicijalizirati s točnim podacima', () => {
    expect(wrapper.vm.clients).toEqual([]);
    expect(wrapper.vm.columns).toEqual([
      { name: 'ime_korisnika', required: true, label: 'Ime klijenta', align: 'left', field: 'ime_korisnika' },
      { name: 'prezime_korisnika', required: true, label: 'Prezime klijenta', align: 'left', field: 'prezime_korisnika' },
      { name: 'kontakt_korisnika', required: true, label: 'Kontakt broj', align: 'left', field: 'kontakt_korisnika' },
      { name: 'edit', label: '', align: 'center', sortable: false },
      { name: 'delete', label: '', align: 'center', sortable: false }
    ]);
  });

  it('treba ići na stranicu TrenerBira kada se pozove metoda editClient', () => {
    const router = wrapper.vm.$router;
    const pushSpy = jest.spyOn(router, 'push');
    const docId = 'client-1';

    wrapper.vm.editClient(docId);

    expect(pushSpy).toHaveBeenCalledWith({ name: 'TrenerBira', params: { id: docId } });
  });

  it('treba izbrisati klijenta kada se pozove metoda deleteClient', async () => {
    const authInstance = wrapper.vm.$firebase.auth();
    const currentUser = { uid: 'trainer-1' };
    jest.spyOn(authInstance, 'currentUser', 'get').mockReturnValue(currentUser);

    const deleteDocSpy = jest.spyOn(wrapper.vm.$firebase.firestore, 'deleteDoc').mockResolvedValue();
    const clientRef = { path: 'path/to/client' };
    jest.spyOn(wrapper.vm.$firebase.firestore, 'doc').mockReturnValue(clientRef);

    const clients = [{ docId: 'client-1' }, { docId: 'client-2' }];
    wrapper.vm.clients = clients;

    await wrapper.vm.deleteClient('client-1');

    expect(wrapper.vm.clients).toEqual([{ docId: 'client-2' }]);
    expect(deleteDocSpy).toHaveBeenCalledWith(clientRef);
  });

  it('ne bi trebao brisati klijenta ako nema autentificiranog korisnika', async () => {
    const authInstance = wrapper.vm.$firebase.auth();
    jest.spyOn(authInstance, 'currentUser', 'get').mockReturnValue(null);

    const deleteDocSpy = jest.spyOn(wrapper.vm.$firebase.firestore, 'deleteDoc').mockResolvedValue();
    const clientRef = { path: 'path/to/client' };
    jest.spyOn(wrapper.vm.$firebase.firestore, 'doc').mockReturnValue(clientRef);

    const clients = [{ docId: 'client-1' }, { docId: 'client-2' }];
    wrapper.vm.clients = clients;

    await wrapper.vm.deleteClient('client-1');

    expect(wrapper.vm.clients).toEqual(clients);
    expect(deleteDocSpy).not.toHaveBeenCalled();
  });

  it('treba rješavati pogreške prilikom brisanja klijenta', async () => {
    const authInstance = wrapper.vm.$firebase.auth();
    const currentUser = { uid: 'trainer-1' };
    jest.spyOn(authInstance, 'currentUser', 'get').mockReturnValue(currentUser);

    const error = new Error('Delete failed');
    const deleteDocSpy = jest.spyOn(wrapper.vm.$firebase.firestore, 'deleteDoc').mockRejectedValue(error);
    const clientRef = { path: 'path/to/client' };
    jest.spyOn(wrapper.vm.$firebase.firestore, 'doc').mockReturnValue(clientRef);

    const clients = [{ docId: 'client-1' }, { docId: 'client-2' }];
    wrapper.vm.clients = clients;

    await wrapper.vm.deleteClient('client-1');

    expect(wrapper.vm.clients).toEqual(clients);
    expect(console.error).toHaveBeenCalledWith('Error deleting client:', error);
  });

  it('treba dohvatiti i prikazati popis klijenata', async () => {
    const clientsData = [
      { id: 'client-1', ime_korisnika: 'John', prezime_korisnika: 'Doe', kontakt_korisnika: '123456789' },
      { id: 'client-2', ime_korisnika: 'Jane', prezime_korisnika: 'Smith', kontakt_korisnika: '987654321' }
    ];

    const getDocsSpy = jest.spyOn(wrapper.vm.$firebase.firestore, 'getDocs').mockResolvedValue(clientsData);

    await wrapper.vm.fetchClients();

    expect(getDocsSpy).toHaveBeenCalled();
    expect(wrapper.vm.clients).toEqual(clientsData);
  });

  it('treba ažurirati popis klijenata kada se promijeni stanje provjere autentičnosti', async () => {
    const clientsData = [
      { id: 'client-1', ime_korisnika: 'John', prezime_korisnika: 'Doe', kontakt_korisnika: '123456789' },
      { id: 'client-2', ime_korisnika: 'Jane', prezime_korisnika: 'Smith', kontakt_korisnika: '987654321' }
    ];

    const getDocsSpy = jest.spyOn(wrapper.vm.$firebase.firestore, 'getDocs').mockResolvedValue(clientsData);

    await wrapper.vm.$firebase.auth().emit('authStateChanged', { uid: 'trainer-1' });

    expect(getDocsSpy).toHaveBeenCalled();
    expect(wrapper.vm.clients).toEqual(clientsData);
  });

  it('treba ažurirati popis klijenata kada se klijent doda ili izbriše iz baze podataka', async () => {
    const onSnapshotMock = jest.fn();
    const querySnapshotMock = {
      docChanges: () => [
        { type: 'added', doc: { id: 'client-1', ime_korisnika: 'John', prezime_korisnika: 'Doe', kontakt_korisnika: '123456789' } },
        { type: 'removed', doc: { id: 'client-2', ime_korisnika: 'Jane', prezime_korisnika: 'Smith', kontakt_korisnika: '987654321' } }
      ]
    };

    jest.spyOn(wrapper.vm.$firebase.firestore, 'collection').mockReturnValue({ onSnapshot: onSnapshotMock });

    await wrapper.vm.watchClientChanges();

    expect(onSnapshotMock).toHaveBeenCalled();
    expect(wrapper.vm.clients).toEqual([
      { docId: 'client-1', ime_korisnika: 'John', prezime_korisnika: 'Doe', kontakt_korisnika: '123456789' }
    ]);
  });

  it('trebao bi otići na stranicu TrenerBira s ispravnim parametrom kada se klikne gumb za uređivanje', async () => {
    const router = wrapper.vm.$router;
    const pushSpy = jest.spyOn(router, 'push');
    const docId = 'client-1';

    await wrapper.vm.$nextTick();

    const editButton = wrapper.find('.edit-button');
    await editButton.trigger('click');

    expect(pushSpy).toHaveBeenCalledWith({ name: 'TrenerBira', params: { id: docId } });
  });

  it('treba pozvati metodu deleteClient kada se klikne gumb za brisanje', async () => {
    const deleteClientSpy = jest.spyOn(wrapper.vm, 'deleteClient');
    const docId = 'client-1';

    await wrapper.vm.$nextTick();

    const deleteButton = wrapper.find('.delete-button');
    await deleteButton.trigger('click');

    expect(deleteClientSpy).toHaveBeenCalledWith(docId);
  });
});