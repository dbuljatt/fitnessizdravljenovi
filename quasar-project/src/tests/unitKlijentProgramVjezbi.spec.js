import { mount } from '@vue/test-utils';
import KlijentProgramVjezbi from 'src/pages/KlijentProgramVjezbi.vue';

describe('KlijentProgramVjezbi', () => {
  it('prikazuje vježbe za klijenta', async () => {
    const authMock = {
      onAuthStateChanged: jest.fn((auth, callback) => {
        // Simuliraj da je klijent ulogiran
        callback({ email: 'klijent@example.com' });
      }),
    };
    const routerMock = {
      push: jest.fn(),
    };
    const dbMock = {
      collection: jest.fn(() => ({
        doc: jest.fn(() => ({
          collection: jest.fn(() => ({
            doc: jest.fn(() => ({
              collection: jest.fn(() => ({
                getDocs: jest.fn(() => ({
                  docs: [
                    { data: () => ({ days: { Ponedjeljak: [] } }) },
                    { data: () => ({ days: { Utorak: [] } }) },
                  ],
                })),
              })),
            })),
          })),
        })),
      })),
    };
    const wrapper = mount(KlijentProgramVjezbi, {
      global: {
        mocks: {
          $auth: authMock,
          $router: routerMock,
          db: dbMock,
        },
      },
    });

    // Provjeri je li metoda onAuthStateChanged pozvana
    expect(authMock.onAuthStateChanged).toHaveBeenCalled();

    // Provjeri je li korisnik ulogiran
    expect(wrapper.vm.client).toEqual({ email: 'klijent@example.com' });

    // Provjeri je li pozvana metoda za dohvaćanje vježbi
    expect(dbMock.collection).toHaveBeenCalled();

    // Provjeri je li prikazana stranica s vježbama
    expect(wrapper.find('.klijent-program-vjezbi').exists()).toBe(true);
    expect(wrapper.find('.q-page').exists()).toBe(true);
    expect(wrapper.find('.exercise-item').exists()).toBe(true);
  });

  it('preusmjerava na prijavu ako klijent nije ulogiran', async () => {
    const authMock = {
      onAuthStateChanged: jest.fn((auth, callback) => {
        // Simuliraj da klijent nije ulogiran
        callback(null);
      }),
    };
    const routerMock = {
      push: jest.fn(),
    };
    const wrapper = mount(KlijentProgramVjezbi, {
      global: {
        mocks: {
          $auth: authMock,
          $router: routerMock,
        },
      },
    });

    // Provjeri je li metoda onAuthStateChanged pozvana
    expect(authMock.onAuthStateChanged).toHaveBeenCalled();

    // Provjeri je li korisnik preusmjeren na prijavu
    expect(routerMock.push).toHaveBeenCalledWith('/login');
  });

//testiranje refresha
it('refresha stranicu kad je klinut gumb za refresh', () => {
    const locationReloadMock = jest.fn();
    global.location.reload = locationReloadMock;

    const wrapper = mount(KlijentProgramVjezbi);
    wrapper.find('.q-btn[label="Osvježi"]').trigger('click');

    expect(locationReloadMock).toHaveBeenCalled();
  });
});