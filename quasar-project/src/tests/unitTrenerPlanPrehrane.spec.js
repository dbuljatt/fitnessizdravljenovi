import { mount } from '@vue/test-utils';
import TrenerPlanPrehrane from 'src/pages/TrenerPlanPrehrane.vue';

describe('TrenerPlanPrehrane', () => {
  it('provjerava ispravnu validaciju obroka', () => {
    const wrapper = mount(TrenerPlanPrehrane);
    const meal = {
      name: 'Doručak',
      items: [
        { name: 'Jelo 1', description: 'Opis 1' },
        { name: '', description: 'Opis 2' },
        { name: 'Jelo 3', description: '' },
      ],
    };

    const isValid = wrapper.vm.isMealValid(meal);

    expect(isValid).toBe(false); // Očekuje se da validacija obroka bude neuspješna jer jedno jelo nema naziv, a drugo nema opis
  });

  it('provjerava prazan obrok', () => {
    const wrapper = mount(TrenerPlanPrehrane);
    const meal = {
      name: 'Doručak',
      items: [],
    };

    const isEmpty = wrapper.vm.isMealEmpty(meal);

    expect(isEmpty).toBe(true); // Očekuje se da obrok bude prazan jer nema nijednog jela
  });

  it('dodaje stavku obroka', () => {
    const wrapper = mount(TrenerPlanPrehrane);
    const day = { name: 'Ponedjeljak', meals: [{ name: 'Doručak', items: [] }] };
    const meal = day.meals[0];

    wrapper.vm.addMealItem(day, meal);

    expect(meal.items).toHaveLength(1); // Očekuje se da je dodana jedna stavka u obrok
    expect(meal.isValid).toBe(false); // Očekuje se da validacija obroka bude neuspješna jer je dodana prazna stavka
    expect(meal.isMealEmpty).toBe(false); // Očekuje se da obrok više nije prazan
  });

  it('sprema plan prehrane', async () => {
    const wrapper = mount(TrenerPlanPrehrane);
    const day = { name: 'Ponedjeljak', meals: [{ name: 'Doručak', items: [] }] };
    const meal = day.meals[0];
    const dayIndex = 0;
    const mealIndex = 0;

    // Mockanje metoda i objekata potrebnih za spremanje plana prehrane
    wrapper.vm.user = { uid: 'neki-trainer-id' };
    wrapper.vm.route = { params: { id: 'neki-client-id' } };
    wrapper.vm.db = {
      collection: jest.fn().mockReturnValue({
        doc: jest.fn().mockReturnValue({
          updateDoc: jest.fn(),
          getDoc: jest.fn().mockReturnValue({ exists: false }),
          setDoc: jest.fn(),
        }),
      }),
    };
    wrapper.vm.router = { push: jest.fn() };

    await wrapper.vm.submitMealPlan(day, meal, dayIndex, mealIndex);

    expect(wrapper.vm.db.collection).toHaveBeenCalledWith('Treneri', 'neki-trainer-id', 'Clients', 'neki-client-id', 'PlanPrehrane');
    expect(wrapper.vm.db.collection().doc).toHaveBeenCalledWith('Dan 1');
    expect(wrapper.vm.db.collection().doc().getDoc).toHaveBeenCalled();
    expect(wrapper.vm.db.collection().doc().updateDoc).toHaveBeenCalledWith({
      'meals.0': { name: 'Doručak', items: [] },
    });
    expect(meal.items).toHaveLength(0); // Očekuje se da su sve stavke obroka izbrisane nakon spremanja
    expect(day.isValid).toBe(false); // Očekuje se da validacija dana bude neuspješna jer su svi obroci prazni
    expect(day.meals[0].isMealEmpty).toBe(true); // Očekuje se da je obrok prazan nakon spremanja
    
  });

});
