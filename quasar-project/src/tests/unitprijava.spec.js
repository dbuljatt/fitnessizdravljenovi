import { shallowMount } from '@vue/test-utils';
import Prijava_m from 'src/pages/Prijava_m.vue';

describe('Prijava_m', () => {
  it('ispravno prikazuje', () => {
    const wrapper = shallowMount(Prijava_m);
    expect(wrapper.exists()).toBe(true);
  });

  it('provjerava odabir uloge', () => {
    const wrapper = shallowMount(Prijava_m);
    expect(wrapper.vm.validateRole('')).toBe('Morate odabrati ulogu'); // Neuspješni scenarij: nije odabrana uloga
    expect(wrapper.vm.validateRole('Trener/ica')).toBe(true); // Uspješni scenarij: odabrana je uloga "Trener/ica"
    expect(wrapper.vm.validateRole('Klijent/ica')).toBe(true); // Uspješni scenarij: odabrana je uloga "Klijent/ica"
  });

  it('dohvaća ulogu korisnika iz Firestorea', async () => {
    const wrapper = shallowMount(Prijava_m);
    const userId = 'testUserId';
    const mockGetFirestore = jest.fn().mockReturnValue({
      doc: jest.fn().mockReturnValueOnce({
        get: jest.fn().mockResolvedValueOnce({ exists: true })
      }),
      getDoc: jest.fn().mockResolvedValueOnce({ exists: false })
    });
    wrapper.vm.getUserRoleFromFirestore = jest.fn(wrapper.vm.getUserRoleFromFirestore);
    wrapper.vm.getUserRoleFromFirestore.mockImplementationOnce(mockGetFirestore);

    const role = await wrapper.vm.getUserRoleFromFirestore(userId);

    expect(role).toBe('Trener/ica');
    expect(mockGetFirestore).toHaveBeenCalled();
    expect(mockGetFirestore).toHaveBeenCalledWith('Treneri', userId);
  });

  it('uspješno izvršava prijavu za trenera', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockResolvedValueOnce({
      user: {
        uid: 'testUserId'
      }
    });
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    const mockDispatch = jest.fn();
    const mockRouter = {
      push: jest.fn()
    };
    const mockStore = {
      dispatch: mockDispatch
    };
    const mockGetFirestore = jest.fn().mockReturnValue({
      doc: jest.fn().mockReturnValueOnce({
        get: jest.fn().mockResolvedValueOnce({ exists: true })
      }),
      getDoc: jest.fn().mockResolvedValueOnce({ exists: false })
    });
    wrapper.vm.$router = mockRouter;
    wrapper.vm.$store = mockStore;
    wrapper.vm.getAuth = mockGetAuth;
    wrapper.vm.getFirestore = mockGetFirestore;

    // Uspješni scenarij: Simuliranje unosa valjanih vjerodajnica trenera
    wrapper.vm.email = 'trener@example.com';
    wrapper.vm.password = 'ispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith('/trener/PocetnaTrener');
  });

  it('uspješno izvršava prijavu za klijenta', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockResolvedValueOnce({
      user: {
        uid: 'testUserId'
      }
    });
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    const mockDispatch = jest.fn();
    const mockRouter = {
      push: jest.fn()
    };
    const mockStore = {
      dispatch: mockDispatch
    };
    const mockGetFirestore = jest.fn().mockReturnValue({
      doc: jest.fn().mockReturnValueOnce({
        get: jest.fn().mockResolvedValueOnce({ exists: true })
      }),
      getDoc: jest.fn().mockResolvedValueOnce({ exists: true })
    });
    wrapper.vm.$router = mockRouter;
    wrapper.vm.$store = mockStore;
    wrapper.vm.getAuth = mockGetAuth;
    wrapper.vm.getFirestore = mockGetFirestore;

    // Uspješni scenarij: Simuliranje unosa valjanih vjerodajnica klijenta
    wrapper.vm.email = 'klijent@example.com';
    wrapper.vm.password = 'ispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).toHaveBeenCalled();
    expect(mockDispatch).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith('/klijent/PocetnaKlijent');
  });

  it('obrađuje prazan email pri prijavi', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockResolvedValueOnce({
      user: {
        uid: 'testUserId'
      }
    });
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    wrapper.vm.getAuth = mockGetAuth;

    // Neuspješni scenarij: Simuliranje praznog unosa e-pošte
    wrapper.vm.email = '';
    wrapper.vm.password = 'ispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).not.toHaveBeenCalled();
    expect(wrapper.vm.error).toBe('Unesite svoju email adresu i lozinku.');
  });

  it('obrađuje praznu lozinku pri prijavi', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockResolvedValueOnce({
      user: {
        uid: 'testUserId'
      }
    });
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    wrapper.vm.getAuth = mockGetAuth;

    // Neuspješni scenarij: Simuliranje praznog unosa lozinke
    wrapper.vm.email = 'klijent@example.com';
    wrapper.vm.password = '';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).not.toHaveBeenCalled();
    expect(wrapper.vm.error).toBe('Unesite svoju email adresu i lozinku.');
  });

  it('obrađuje grešku pri prijavi s neispravnim vjerodajnicama', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockRejectedValueOnce(new Error('Invalid credentials'));
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    wrapper.vm.getAuth = mockGetAuth;

    // Neuspješni scenarij: Simuliranje unosa nevaljanih vjerodajnica
    wrapper.vm.email = 'klijent@example.com';
    wrapper.vm.password = 'neispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).toHaveBeenCalled();
    expect(wrapper.vm.error).toBe('Provjerite svoje korisničke podatke i pokušajte ponovno.');
  });

  it('obrađuje grešku pri prijavi s drugim greškama', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockRejectedValueOnce(new Error('Some other error'));
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    wrapper.vm.getAuth = mockGetAuth;

    // Neuspješni scenarij: Simuliranje greške tijekom prijave s nevaljanim vjerodajnicama
    wrapper.vm.email = 'klijent@example.com';
    wrapper.vm.password = 'ispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).toHaveBeenCalled();
    expect(wrapper.vm.error).toBe('Došlo je do pogreške. Pokušajte ponovno kasnije.');
  });

  it('obrađuje slučaj kada uloga korisnika nije pronađena u Firestoreu', async () => {
    const wrapper = shallowMount(Prijava_m);
    const mockSignInWithEmailAndPassword = jest.fn().mockResolvedValueOnce({
      user: {
        uid: 'testUserId'
      }
    });
    const mockGetAuth = jest.fn().mockReturnValue({
      signInWithEmailAndPassword: mockSignInWithEmailAndPassword
    });
    const mockGetFirestore = jest.fn().mockReturnValue({
      doc: jest.fn().mockReturnValueOnce({
        get: jest.fn().mockResolvedValueOnce({ exists: false })
      })
    });
    wrapper.vm.getAuth = mockGetAuth;
    wrapper.vm.getFirestore = mockGetFirestore;

    // Neuspješni scenarij: Simuliranje prijave s valjanim klijentskim vjerodajnicama koje nisu povezane s ulogom
    wrapper.vm.email = 'klijent@example.com';
    wrapper.vm.password = 'ispravnaLozinka';

    await wrapper.vm.login();

    expect(mockSignInWithEmailAndPassword).toHaveBeenCalled();
    expect(wrapper.vm.error).toBe('Pristup odbijen. Kontaktirajte administratora.');
  });

  it('provjerava ispravan format emaila', () => {
    const wrapper = shallowMount(Prijava_m);
    const validEmail = 'test@example.com';
    const invalidEmail = 'invalid_email';

    // Uspješni scenarij: Validacija ispravnog formata e-pošte
    expect(wrapper.vm.validateEmailFormat(validEmail)).toBe(true);

    // Neuspješni scenarij: Validacija neispravnog formata e-pošte
    expect(wrapper.vm.validateEmailFormat(invalidEmail)).toBe('Neispravan format email adrese');
  });

  it('provjerava duljinu lozinke', () => {
    const wrapper = shallowMount(Prijava_m);

    // Neuspješni scenarij: Validacija prazne lozinke
    expect(wrapper.vm.validatePassword('')).toBe('Unesite svoju lozinku.');

    // Neuspješni scenarij: Validacija prekratke lozinke
    expect(wrapper.vm.validatePassword('short')).toBe('Lozinka mora imati barem 6 znakova.');

    // Uspješni scenarij: Validacija valjane duljine lozinke
    expect(wrapper.vm.validatePassword('password123')).toBe(true);
  });
});
