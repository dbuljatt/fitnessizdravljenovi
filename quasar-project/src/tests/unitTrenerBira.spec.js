import { shallowMount } from '@vue/test-utils';
import TrenerBira from 'src/pages/TrenerBira.vue';

describe('TrenerBira', () => {
  let wrapper;
  let mockRoute;
  let mockRouter;

  beforeEach(() => {
    mockRoute = {
      params: { id: 'client-1' },
    };

    mockRouter = {
      push: jest.fn(),
    };

    wrapper = shallowMount(TrenerBira, {
      global: {
        mocks: {
          $route: mockRoute,
          $router: mockRouter,
        },
        stubs: ['router-link'],
      },
    });
  });

  afterEach(() => {
    wrapper.unmount();
  });

  it('treba prijeći na "Program vježbi" kada se klikne na odgovarajuću karticu  ', async () => {
    const navigateToProgramVjezbiSpy = jest.spyOn(wrapper.vm, 'navigateToProgramVjezbi');

    const programVjezbiCard = wrapper.find('.card-container:nth-child(1)');
    await programVjezbiCard.trigger('click');

    expect(navigateToProgramVjezbiSpy).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerProgramVjezbi', params: { id: 'client-1' } });
  });

  it('treba prijeći na "Plan prehrane" kada se klikne na odgovarajuću karticu', async () => {
    const navigateToTrenerPlanPrehraneSpy = jest.spyOn(wrapper.vm, 'navigateToTrenerPlanPrehrane');

    const planPrehraneCard = wrapper.find('.card-container:nth-child(2)');
    await planPrehraneCard.trigger('click');

    expect(navigateToTrenerPlanPrehraneSpy).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerPlanPrehrane', params: { id: 'client-1' } });
  });

  it('treba prijeći na "Podaci o klijentu" kada se klikne na odgovarajuću karticu', async () => {
    const navigateToPodacioKlijentuSpy = jest.spyOn(wrapper.vm, 'navigateToPodacioKlijentu');

    const podaciKlijentuCard = wrapper.find('.card-container:nth-child(3)');
    await podaciKlijentuCard.trigger('click');

    expect(navigateToPodacioKlijentuSpy).toHaveBeenCalled();
    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerPodacioKlijentu', params: { id: 'client-1' } });
  });

  it('treba dohvatiti ime klijenta pri montiranju komponente', async () => {
    const fetchClientNameSpy = jest.spyOn(wrapper.vm, 'fetchClientName');

    await wrapper.vm.$options.beforeMount.call(wrapper.vm);

    expect(fetchClientNameSpy).toHaveBeenCalled();
  });

  it('treba ispravno postaviti ime klijenta na temelju dohvaćenih podataka', async () => {
    const clientName = 'John Doe';
    const mockGetAuth = jest.fn(() => ({
      currentUser: { uid: 'trainer-1' },
    }));
    const mockGetDoc = jest.fn(() => ({
      exists: jest.fn(() => true),
      data: jest.fn(() => ({
        ime_korisnika: 'John',
        prezime_korisnika: 'Doe',
      })),
    }));

    jest.mock('firebase/auth', () => ({
      getAuth: mockGetAuth,
      onAuthStateChanged: jest.fn(),
    }));

    jest.mock('firebase/firestore', () => ({
      getDoc: mockGetDoc,
      doc: jest.fn(),
      db: jest.fn(),
    }));

    await wrapper.vm.fetchClientName();

    expect(wrapper.vm.clientName).toBe(clientName);
    expect(wrapper.find('h2').text()).toBe(clientName);
  });

  it('treba ići na "Program vježbi" s točnim ID-om klijenta kada se klikne na karticu "Program vježbi"', async () => {
    const programVjezbiCard = wrapper.find('.card-container:nth-child(1)');
    await programVjezbiCard.trigger('click');

    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerProgramVjezbi', params: { id: 'client-1' } });
  });

  it('treba ići na "Plan prehrane" s točnim ID-om klijenta kada se klikne na karticu "Plan prehrane".', async () => {
    const planPrehraneCard = wrapper.find('.card-container:nth-child(2)');
    await planPrehraneCard.trigger('click');

    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerPlanPrehrane', params: { id: 'client-1' } });
  });

  it('treba ići na "Podaci o klijentu" s točnim ID-om klijenta kada se klikne na karticu "Podaci o klijentu"', async () => {
    const podaciKlijentuCard = wrapper.find('.card-container:nth-child(3)');
    await podaciKlijentuCard.trigger('click');

    expect(mockRouter.push).toHaveBeenCalledWith({ name: 'TrenerPodacioKlijentu', params: { id: 'client-1' } });
  });

  it('ne bi trebao postaviti ime klijenta kada korisnik nije autentificiran', async () => {
    const mockGetAuth = jest.fn(() => ({
      currentUser: null,
    }));

    jest.mock('firebase/auth', () => ({
      getAuth: mockGetAuth,
    }));

    await wrapper.vm.fetchClientName();

    expect(wrapper.vm.clientName).toBe('');
    expect(wrapper.find('h2').exists()).toBe(false);
  });

  it('ne bi trebao postaviti ime klijenta kada dokument klijenta ne postoji', async () => {
    const mockGetAuth = jest.fn(() => ({
      currentUser: { uid: 'trainer-1' },
    }));
    const mockGetDoc = jest.fn(() => ({
      exists: jest.fn(() => false),
    }));

    jest.mock('firebase/auth', () => ({
      getAuth: mockGetAuth,
    }));

    jest.mock('firebase/firestore', () => ({
      getDoc: mockGetDoc,
      doc: jest.fn(),
      db: jest.fn(),
    }));

    await wrapper.vm.fetchClientName();

    expect(wrapper.vm.clientName).toBe('');
    expect(wrapper.find('h2').exists()).toBe(false);
  });
});
