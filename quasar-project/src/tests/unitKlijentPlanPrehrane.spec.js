import { mount } from '@vue/test-utils';
import KlijentPlanPrehrane from 'src/pages/KlijentPlanPrehrane.vue';

describe('KlijentPlanPrehrane', () => {
  it('prikazuje plan prehrane za odabrane dane', async () => {
    const wrapper = mount(KlijentPlanPrehrane);

    // Simuliraj odabir prvog dana
    await wrapper.find('.q-expansion-item').trigger('click');

    // Očekuj da je prvi dan označen kao odabran
    expect(wrapper.vm.days[0].selected).toBe(true);

    // Očekuj da su prikazani podaci o jelima za prvi dan
    expect(wrapper.find('.meal-item').exists()).toBe(true);

    // Simuliraj odabir drugog dana
    await wrapper.find('.q-expansion-item:nth-child(2)').trigger('click');

    // Očekuj da je drugi dan označen kao odabran
    expect(wrapper.vm.days[1].selected).toBe(true);

    // Očekuj da su prikazani podaci o jelima za drugi dan
    expect(wrapper.find('.meal-item').exists()).toBe(true);
  });

  it('ne prikazuje podatke o jelima za neodabrane dane', async () => {
    const wrapper = mount(KlijentPlanPrehrane);

    // Očekuj da nijedan dan nije odabran
    expect(wrapper.vm.days.every((day) => !day.selected)).toBe(true);

    // Očekuj da nema prikazanih podataka o jelima
    expect(wrapper.find('.meal-item').exists()).toBe(false);
  });

  it('provjerava je li klijent ulogiran', async () => {
    const authMock = {
      onAuthStateChanged: jest.fn((auth, callback) => {
        // Simuliraj da je klijent ulogiran
        callback({ email: 'marta@veleri.hr' });
      }),
    };
    const wrapper = mount(KlijentPlanPrehrane, {
      global: {
        mocks: {
          $auth: authMock,
        },
      },
    });

    // Provjera je li metoda onAuthStateChanged pozvana
    expect(authMock.onAuthStateChanged).toHaveBeenCalled();

    // Provjera je li korisnik ulogiran
    expect(wrapper.vm.user).toEqual({ email: 'marta@veleri.hr' });

    // Provjera je li prikazana stranica bez preusmjeravanja
    expect(wrapper.find('.klijent-plan-prehrane').exists()).toBe(true);
    expect(wrapper.find('.q-page').exists()).toBe(true);
  });
});