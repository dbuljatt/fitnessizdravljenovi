import { mount } from '@vue/test-utils';
import KlijentLayout from 'src/layouts/KlijentLayout.vue';

describe('KlijentLayout', () => {
  test('prikazuje avatara baziranom na spolu', async () => {
    const mockUser = {
      uid: 'user-123',
      email: 'test@example.com',
      shape: 'Muško'
    };
    const wrapper = mount(KlijentLayout, {
      global: {
        mocks: {
          $user: mockUser
        }
      }
    });

    // Provjeri da li se prikazuje avatar za muško
    const maleAvatar = wrapper.find('.q-avatar[src="https://cdn.quasar.dev/img/boy-avatar.png"]');
    expect(maleAvatar.exists()).toBe(true);

    // Promijeni spol na žensko
    mockUser.shape = 'Žensko';

    // Pričekaj zahtjev za ažuriranjem avatara
    await wrapper.vm.$nextTick();

    // Provjeri da li se prikazuje avatar za žensko
    const femaleAvatar = wrapper.find('.q-avatar[src="https://img.freepik.com/premium-vector/face-cute-girl-avatar-young-girl-portrait-vector-flat-illustration_192760-82.jpg?w=2000"]');
    expect(femaleAvatar.exists()).toBe(true);
  });

  test('odlogira se na klik', async () => {
    const mockUser = {
      uid: 'user-123',
      email: 'test@example.com',
      shape: 'Muško'
    };
    const mockSignOut = jest.fn();
    const $router = {
      push: jest.fn()
    };
    const wrapper = mount(KlijentLayout, {
      global: {
        mocks: {
          $user: mockUser,
          $router: $router
        },
        stubs: ['EssentialLink']
      }
    });

    // Pronađi odjavu korisnika i simuliraj klik
    const signOutButton = wrapper.find('.text-grey-10:contains("Odjava")');
    await signOutButton.trigger('click');

    // Provjeri da li se korisnik odjavio
    expect(mockSignOut).toHaveBeenCalled();

    // Provjeri da li je preusmjeravanje na login stranicu izvršeno
    expect($router.push).toHaveBeenCalledWith('/login');
  });
});