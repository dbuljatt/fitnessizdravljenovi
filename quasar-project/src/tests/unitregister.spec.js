import { shallowMount } from '@vue/test-utils';
import Registracija_m from 'src/pages/Registracija_m.vue';

describe('Registracija_m', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Registracija_m);
  });

  it('validira ime isrpavno', () => {
    expect(wrapper.vm.validateName('John')).toBe(true);
    expect(wrapper.vm.validateName('123')).toBe('Ime ne smije imati brojke.');
    expect(wrapper.vm.validateName('')).toBe('Ime je obavezno.');
  });

  it('validira preime ispravno', () => {
    expect(wrapper.vm.validateSurname('Doe')).toBe(true);
    expect(wrapper.vm.validateSurname('123')).toBe('Prezime ne smije imati brojke.');
    expect(wrapper.vm.validateSurname('')).toBe('Prezime je obavezno.');
  });

  it('validira username ispravno', () => {
    expect(wrapper.vm.validateUsername('johndoe')).toBe(true);
    expect(wrapper.vm.validateUsername('john doe')).toBe('Korisničko ime ne smije imati specijalne znakove ili razmak.');
    expect(wrapper.vm.validateUsername('')).toBe('Korisničko ime je obavezno.');
  });

  it('validira email ispravno', () => {
    expect(wrapper.vm.validateEmail('john@example.com')).toBe(true);
    expect(wrapper.vm.validateEmail('john@')).toBe('Nevažeća email adresa');
    expect(wrapper.vm.validateEmail('')).toBe('Nevažeća email adresa');
  });

  it('validira lozinku ispravno', () => {
    expect(wrapper.vm.validatePassword('Abcdef12')).toBe(true);
    expect(wrapper.vm.validatePassword('abcdef12')).toBe('Lozinka mora sadržavati barem jedno veliko slovo');
    expect(wrapper.vm.validatePassword('12345678')).toBe('Lozinka mora sadržavati barem jedno malo slovo');
    expect(wrapper.vm.validatePassword('Abcdefgh')).toBe('Lozinka mora sadržavati barem jednu brojku');
    expect(wrapper.vm.validatePassword('')).toBe('Lozinka je obavezna');
  });

  it('validira broj mob', () => {
    expect(wrapper.vm.validatePhoneNumber('1234567890')).toBe(true);
    expect(wrapper.vm.validatePhoneNumber('abcdefghij')).toBe('Broj telefona ne smije sadržavati slova');
    expect(wrapper.vm.validatePhoneNumber('12345')).toBe('Broj telefona mora imati točno 10 brojki');
    expect(wrapper.vm.validatePhoneNumber('')).toBe('Broj telefona je obavezan');
  });

  it('validira datum rodenja', () => {
    expect(wrapper.vm.validateDateOfBirth('1990-01-01')).toBe(true);
    expect(wrapper.vm.validateDateOfBirth('2100-01-01')).toBe('Datum rođenja ne može biti u budućnosti');
    expect(wrapper.vm.validateDateOfBirth('')).toBe('Datum rođenja je obavezan');
  });

  it('validira spol', () => {
    expect(wrapper.vm.validateGender('male')).toBe(true);
    expect(wrapper.vm.validateGender('')).toBe('Morate odabrati spol');
  });

  it('validira ulogu', () => {
    expect(wrapper.vm.validateRole('Trener/ica')).toBe(true);
    expect(wrapper.vm.validateRole('')).toBe('Morate odabrati ulogu');
  });

  it('validira velicinu', () => {
    expect(wrapper.vm.validateHeight('180')).toBe(true);
    expect(wrapper.vm.validateHeight('abc')).toBe('Visina mora biti izražena brojkama');
    expect(wrapper.vm.validateHeight('')).toBe('Visina je obavezna');
  });

  it('validates tezinu', () => {
    expect(wrapper.vm.validateWeight('70')).toBe(true);
    expect(wrapper.vm.validateWeight('xyz')).toBe('Težina mora biti izražena brojkama');
    expect(wrapper.vm.validateWeight('')).toBe('Težina je obavezna');
  });

  it('uspjesna registracija', async () => {
    
    const createUserWithEmailAndPassword = jest.fn().mockResolvedValue({});
    const auth = { createUserWithEmailAndPassword };
    const firebase = { auth };
    wrapper = shallowMount(Registracija_m, {
      global: {
        provide: {
          firebase,
        },
      },
    });

    
    wrapper.vm.email = 'john@example.com';
    wrapper.vm.password = 'Abcdef12';
    wrapper.vm.ime_korisnika = 'John';
    wrapper.vm.prezime_korisnika = 'Doe';
    wrapper.vm.role = 'Trener/ica';
    wrapper.vm.username_korisnika = 'johndoe';
    wrapper.vm.kontakt_korisnika = '1234567890';
    wrapper.vm.datumrod_korisnika = '1990-01-01';
    wrapper.vm.shape = 'male';
    wrapper.vm.visina_korisnika = '180';
    wrapper.vm.tezina_korisnika = '70';

   
    await wrapper.vm.register();

    
    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      'john@example.com',
      'Abcdef12'
    );
    expect(wrapper.vm.registrationSuccessful).toBe(true);
    expect(wrapper.vm.registrationError).toBe(null);
  });

  it('nije registria korisnika', async () => {
    
    const createUserWithEmailAndPassword = jest.fn().mockRejectedValue(new Error('Registration failed'));
    const auth = { createUserWithEmailAndPassword };
    const firebase = { auth };
    wrapper = shallowMount(Registracija_m, {
      global: {
        provide: {
          firebase,
        },
      },
    });

    // Postavljanje potrebnih podataka za registraciju
    wrapper.vm.email = 'john@example.com';
    wrapper.vm.password = 'Abcdef12';
    wrapper.vm.ime_korisnika = 'John';
    wrapper.vm.prezime_korisnika = 'Doe';
    wrapper.vm.role = 'Trener/ica';
    wrapper.vm.username_korisnika = 'johndoe';
    wrapper.vm.kontakt_korisnika = '1234567890';
    wrapper.vm.datumrod_korisnika = '1990-01-01';
    wrapper.vm.shape = 'male';
    wrapper.vm.visina_korisnika = '180';
    wrapper.vm.tezina_korisnika = '70';

    // Okidanje register metode
    await wrapper.vm.register();

    //  Provjera da se registracija neuspješno izvršava
    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      'john@example.com',
      'Abcdef12'
    );
    expect(wrapper.vm.registrationSuccessful).toBe(false);
    expect(wrapper.vm.registrationError).toBe('Registration failed');
  });

  it('ne registrira ako posotji taj email', async () => {
    // Mock metode za firebase autentikaciju
    const createUserWithEmailAndPassword = jest.fn().mockRejectedValue({ code: 'auth/email-already-in-use' });
    const auth = { createUserWithEmailAndPassword };
    const firebase = { auth };
    wrapper = shallowMount(Registracija_m, {
      global: {
        provide: {
          firebase,
        },
      },
    });

    // Postavljanje potrebnih podataka za registraciju
    wrapper.vm.email = 'john@example.com';
    wrapper.vm.password = 'Abcdef12';
    wrapper.vm.ime_korisnika = 'John';
    wrapper.vm.prezime_korisnika = 'Doe';
    wrapper.vm.role = 'Trener/ica';
    wrapper.vm.username_korisnika = 'johndoe';
    wrapper.vm.kontakt_korisnika = '1234567890';
    wrapper.vm.datumrod_korisnika = '1990-01-01';
    wrapper.vm.shape = 'male';
    wrapper.vm.visina_korisnika = '180';
    wrapper.vm.tezina_korisnika = '70';

    // Okidanje register metode
    await wrapper.vm.register();

    // Provjeri da registracija ne uspijeva s greškom da je e-mail već u upotrebi.
    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      'john@example.com',
      'Abcdef12'
    );
    expect(wrapper.vm.registrationSuccessful).toBe(false);
    expect(wrapper.vm.registrationError).toBe('Email already in use');
  });

  it('ne registrira ako je netocno nesen mail', async () => {
    // Mock metode za Firebase autentikaciju.
    const createUserWithEmailAndPassword = jest.fn().mockRejectedValue({ code: 'auth/invalid-email' });
    const auth = { createUserWithEmailAndPassword };
    const firebase = { auth };
    wrapper = shallowMount(Registracija_m, {
      global: {
        provide: {
          firebase,
        },
      },
    });

    // Dohvaćanje potrebnih podataka za registraciju
    wrapper.vm.email = 'john@example.com';
    wrapper.vm.password = 'Abcdef12';
    wrapper.vm.ime_korisnika = 'John';
    wrapper.vm.prezime_korisnika = 'Doe';
    wrapper.vm.role = 'Trener/ica';
    wrapper.vm.username_korisnika = 'johndoe';
    wrapper.vm.kontakt_korisnika = '1234567890';
    wrapper.vm.datumrod_korisnika = '1990-01-01';
    wrapper.vm.shape = 'male';
    wrapper.vm.visina_korisnika = '180';
    wrapper.vm.tezina_korisnika = '70';

    // Okidanje register metode
    await wrapper.vm.register();

   // Provjeri da registracija ne uspijeva s greškom neispravne email adrese.
    expect(createUserWithEmailAndPassword).toHaveBeenCalledWith(
      'john@example.com',
      'Abcdef12'
    );
    expect(wrapper.vm.registrationSuccessful).toBe(false);
    expect(wrapper.vm.registrationError).toBe('Invalid email');
  });
});
