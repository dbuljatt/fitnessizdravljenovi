import { mount } from '@vue/test-utils'
import PocetnaTrener from 'src/pages/PocetnaTrener.vue'

describe('PocetnaTrener', () => {
  it('renderira komponentu', () => {
    const wrapper = mount(PocetnaTrener)
    expect(wrapper.exists()).toBe(true)
  })

  it('prikazuje točan broj slajdova u karuselu', () => {
    const wrapper = mount(PocetnaTrener)
    const carouselSlides = wrapper.findAll('.q-carousel .q-carousel-slide')
    expect(carouselSlides.length).toBe(4)
  })

  it('otvara stranicu za fitness u novom tabu kada se klikne', () => {
    const wrapper = mount(PocetnaTrener)
    const openFitnessPageButton = wrapper.find('.q-card .q-btn')
    openFitnessPageButton.trigger('click')
    expect(window.open).toHaveBeenCalledWith('https://www.fitness.com.hr/', '_blank')
  })

  it('sprema novu bilješku kada se klikne na gumb za spremanje', async () => {
    const wrapper = mount(PocetnaTrener)
    const noteTitleInput = wrapper.find('input[label="Unesite naslov..."]')
    const noteContentInput = wrapper.find('textarea[label="Unesite sadržaj..."]')
    const saveNoteButton = wrapper.find('button[label="Spremi bilješku"]')

    noteTitleInput.setValue('Test Naslov')
    noteContentInput.setValue('Test Sadržaj')
    saveNoteButton.trigger('click')
    await wrapper.vm.$nextTick()

    const savedNotes = wrapper.vm.notes
    expect(savedNotes.length).toBe(1)
    expect(savedNotes[0].title).toBe('Test Naslov')
    expect(savedNotes[0].content).toBe('Test Sadržaj')
  })

  it('briše bilješku kada se klikne na gumb za brisanje', async () => {
    const wrapper = mount(PocetnaTrener)

    wrapper.vm.notes.push({ id: 'note1', title: 'Test Naslov', content: 'Test Sadržaj' })
    await wrapper.vm.$nextTick()

    const deleteNoteButton = wrapper.find('button[icon="delete"]')
    deleteNoteButton.trigger('click')
    await wrapper.vm.$nextTick()

    const savedNotes = wrapper.vm.notes
    expect(savedNotes.length).toBe(0)
  })

  it('uređuje bilješku kada se klikne na gumb za uređivanje', async () => {
    const wrapper = mount(PocetnaTrener)
    
    wrapper.vm.notes.push({ id: 'note1', title: 'Stari Naslov', content: 'Stari Sadržaj' })
    await wrapper.vm.$nextTick()

    const editNoteButton = wrapper.find('button[icon="edit"]')
    editNoteButton.trigger('click')
    await wrapper.vm.$nextTick()

    const noteTitleInput = wrapper.find('input[label="Unesite naslov..."]')
    const noteContentInput = wrapper.find('textarea[label="Unesite sadržaj..."]')

    expect(noteTitleInput.element.value).toBe('Stari Naslov')
    expect(noteContentInput.element.value).toBe('Stari Sadržaj')

    noteTitleInput.setValue('Novi Naslov')
    noteContentInput.setValue('Novi Sadržaj')

    const saveNoteButton = wrapper.find('button[label="Spremi bilješku"]')
    saveNoteButton.trigger('click')
    await wrapper.vm.$nextTick()

    const savedNotes = wrapper.vm.notes
    expect(savedNotes.length).toBe(1)
    expect(savedNotes[0].title).toBe('Novi Naslov')
    expect(savedNotes[0].content).toBe('Novi Sadržaj')

  })

  it('treba prikazati poruku o grešci kada je naslov bilješke prazan', async () => {
    const wrapper = mount(PocetnaTrener);
  
    await wrapper.find('[label="Unesite naslov..."]').setValue('');
    await wrapper.find('[label="Unesite sadržaj..."]').setValue('Sadržaj bilješke');
    await wrapper.find('[color="primary"]').trigger('click');
    
    expect(wrapper.find('.q-validation-item-message').text()).toBe('Naslov je obavezan');
  });

  it('treba prikazati poruku o grešci kada je sadržaj bilješke prazan', async () => {
    const wrapper = mount(PocetnaTrener);
  
    await wrapper.find('[label="Unesite naslov..."]').setValue('Naslov bilješke');
    await wrapper.find('[label="Unesite sadržaj..."]').setValue('');
    await wrapper.find('[color="primary"]').trigger('click');
    
    expect(wrapper.find('.q-validation-item-message').text()).toBe('Sadržaj je obavezan');
  });

  it('treba spremiti bilješku kada su uneseni i naslov i sadržaj', async () => {
    const wrapper = mount(PocetnaTrener);
  
    await wrapper.find('[label="Unesite naslov..."]').setValue('Naslov bilješke');
    await wrapper.find('[label="Unesite sadržaj..."]').setValue('Sadržaj bilješke');
    await wrapper.find('[color="primary"]').trigger('click');
    
    expect(wrapper.emitted().saveNote).toBeTruthy();
    expect(wrapper.vm.noteTitle).toBe('');
    expect(wrapper.vm.noteContent).toBe('');
  });

  it('ne uspijeva spremiti bilješku kada je naslov ili sadržaj prazan', async () => {
    const wrapper = mount(PocetnaTrener);
  
    const noteTitleInput = wrapper.find('input[label="Unesite naslov..."]');
    const noteContentInput = wrapper.find('textarea[label="Unesite sadržaj..."]');
  
    noteTitleInput.setValue('');
    noteContentInput.setValue('');
  
    const saveNoteButton = wrapper.find('button[label="Spremi bilješku"]');
    saveNoteButton.trigger('click');
    await wrapper.vm.$nextTick();
  
    const savedNotes = wrapper.vm.notes;
    expect(savedNotes.length).toBe(0);
  });
})
