import { shallowMount } from '@vue/test-utils';
import MojProfilTrener from 'src/pages/MojProfilTrener.vue';

describe('MojProfilTrener', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(MojProfilTrener);
  });

  it('inicijalizira s točnim podacima', () => {
    expect(wrapper.vm.ime_korisnika).toBe('');
    expect(wrapper.vm.prezime_korisnika).toBe('');
    expect(wrapper.vm.kontakt_korisnika).toBe('');
    expect(wrapper.vm.email).toBe('');
    expect(wrapper.vm.shape).toBe('');
    expect(wrapper.vm.tezina_korisnika).toBe('');
    expect(wrapper.vm.visina_korisnika).toBe('');
    expect(wrapper.vm.datumrod_korisnika).toBe('');
    expect(wrapper.vm.editing).toBe(false);
  });

  it('ispravno prebacuje način uređivanja', () => {
    expect(wrapper.vm.editing).toBe(false);
    wrapper.vm.toggleEditing();
    expect(wrapper.vm.editing).toBe(true);
    wrapper.vm.toggleEditing();
    expect(wrapper.vm.editing).toBe(false);
  });

  it('ispravno ažurira korisnički profil', async () => {
    const updateDoc = jest.fn().mockResolvedValue();
    const docRef = jest.fn();
    const currentUser = { uid: 'user1' };
    wrapper.vm.updateDoc = updateDoc;
    wrapper.vm.auth = { currentUser };
    wrapper.vm.doc = docRef;

    wrapper.vm.ime_korisnika = 'John';
    wrapper.vm.prezime_korisnika = 'Doe';
    wrapper.vm.kontakt_korisnika = '123456789';
    wrapper.vm.email = 'john.doe@example.com';
    wrapper.vm.shape = 'M';
    wrapper.vm.tezina_korisnika = '80';
    wrapper.vm.visina_korisnika = '180';
    wrapper.vm.datumrod_korisnika = '1990-01-01';

    await wrapper.vm.updateTrener();

    expect(updateDoc).toHaveBeenCalledWith(
      docRef,
      expect.objectContaining({
        ime_korisnika: 'John',
        prezime_korisnika: 'Doe',
        kontakt_korisnika: '123456789',
        email: 'john.doe@example.com',
        shape: 'M',
        tezina_korisnika: '80',
        visina_korisnika: '180',
        datumrod_korisnika: '1990-01-01',
      })
    );
  });

  it('onemoguci uredivanje ako nije kliknut gumb', () => {
    wrapper.vm.editing = false;
    const inputFields = wrapper.findAll('.form-input');

    inputFields.forEach((field) => {
      expect(field.attributes('readonly')).toBeTruthy();
    });
  });

  it('omoguci kad se klkne na gumb', () => {
    wrapper.vm.editing = true;
    const inputFields = wrapper.findAll('.form-input');

    inputFields.forEach((field) => {
      expect(field.attributes('readonly')).toBeFalsy();
    });
  });

  it('prikazuje spremi gumb kad je u uredivanju', () => {
    wrapper.vm.editing = true;
    const saveButton = wrapper.find('[icon="save"]');

    expect(saveButton.exists()).toBeTruthy();
  });

  it('prikazuje uredi gumb kad nije u uredivanju', () => {
    wrapper.vm.editing = false;
    const editButton = wrapper.find('[icon="edit"]');

    expect(editButton.exists()).toBeTruthy();
  });

  it('ažurira podatke kad se klikne na gumb sremi', async () => {
    
    wrapper.vm.editing = true;
    await wrapper.vm.$nextTick();

    
    const imeInput = wrapper.find('input[type="text"][v-model="ime_korisnika"]');
    imeInput.setValue('New Name');
    
    const prezimeInput = wrapper.find('input[type="text"][v-model="prezime_korisnika"]');
    prezimeInput.setValue('New Last Name');
    
    const kontaktInput = wrapper.find('input[type="text"][v-model="kontakt_korisnika"]');
    kontaktInput.setValue('New Contact');
    
    const emailInput = wrapper.find('input[type="text"][v-model="email"]');
    emailInput.setValue('newemail@example.com');
    
    const shapeInput = wrapper.find('input[type="text"][v-model="shape"]');
    shapeInput.setValue('F');
    
    const tezinaInput = wrapper.find('input[type="text"][v-model="tezina_korisnika"]');
    tezinaInput.setValue('60');
    
    const visinaInput = wrapper.find('input[type="text"][v-model="visina_korisnika"]');
    visinaInput.setValue('170');
    
    const datumrodInput = wrapper.find('input[type="text"][v-model="datumrod_korisnika"]');
    datumrodInput.setValue('1995-05-05');

    // Simulirajte klik na gumb za spremanje
    const saveButton = wrapper.find('[icon="save"]');
    await saveButton.trigger('click');

    // Provjerite jesu li ažurirani podaci ispravni
    expect(wrapper.vm.ime_korisnika).toBe('New Name');
    expect(wrapper.vm.prezime_korisnika).toBe('New Last Name');
    expect(wrapper.vm.kontakt_korisnika).toBe('New Contact');
    expect(wrapper.vm.email).toBe('newemail@example.com');
    expect(wrapper.vm.shape).toBe('F');
    expect(wrapper.vm.tezina_korisnika).toBe('60');
    expect(wrapper.vm.visina_korisnika).toBe('170');
    expect(wrapper.vm.datumrod_korisnika).toBe('1995-05-05');
    
  });

});